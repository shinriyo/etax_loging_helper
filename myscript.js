(function (loadedListener) {
　var doc, readyState;

　doc = document;
　readyState = doc.readyState;

　if (readyState === 'complete' || readyState === 'interactive') {
　　loadedListener();
　} else {
　　doc.addEventListener("DOMContentLoaded", loadedListener, false);
　}
})(function () {
  var getItem = function(key) {
    return localStorage.getItem(key);
  };

  var setItem = function(key, value) {
    localStorage.setItem(key, value);
  };
  // これはpopup.htmlを読んだ瞬間
  // 利用者識別番号
  var myid1 = document.getElementById('myid-1');
  var myid2 = document.getElementById('myid-2');
  var myid3 = document.getElementById('myid-3');
  var myid4 = document.getElementById('myid-4');
  // パスワード
  var mypwd = document.getElementById('mypwd');
  // local storageから取り出しておく
  // 利用者識別番号
  var myid1_save = getItem('myid1_save');
  var myid2_save = getItem('myid2_save');
  var myid3_save = getItem('myid3_save');
  var myid4_save = getItem('myid4_save');
  // パスワード
  var mypwd_save = getItem('mypwd_save');

  // 利用者識別番号を枠に入れておく
  if (typeof myid1_save !== "undefined") {
    myid1.value = myid1_save;
  }

  if (typeof myid2_save !== "undefined") {
    myid2.value = myid2_save;
  }

  if (typeof myid3_save !== "undefined") {
    myid3.value = myid3_save;
  }

  if (typeof myid4_save !== "undefined") {
    myid4.value = myid4_save;
  }

  // パスワードを枠に入れておく
  if (typeof mypwd_save !== "undefined") {
    mypwd.value = mypwd_save;
  }

  // 保存ボタンに処理を入れておく
  var btn = document.getElementById('btn');
  var msg = document.getElementById('msg');

  // クリック時の処理
  btn.addEventListener('click', function() {
    // 利用者識別番号 と パスワードが入ってることをチェック
    if (myid1.value != '' && myid2.value != '' &&
        myid3.value != '' && myid4.value != '' &&
      mypwd.value != '') {
      // 保存する
      setItem('myid1_save', myid1.value);
      setItem('myid2_save', myid2.value);
      setItem('myid3_save', myid3.value);
      setItem('myid4_save', myid4.value);
      setItem('mypwd_save', mypwd.value);

      msg.innerText = 'IDとパスワードを保存しました。';
    } else {
      msg.innerText = 'IDとパスワードを正しく入力して下さい。';
    }
  });

  // 保存ボタンに処理を入れておく
  var etax_btn = document.getElementById('etax-btn');

  // クリック次の処理
  etax_btn.addEventListener('click', function() {
    var a = document.getElementById("maskat_widget_dojo_84");
    if (typeof a === "undefined") {
      // msg.innerText = 'ng';
      // msg.innerText = document.getElementById("maskat_widget_dojo_84").value;
    } else {
      msg.innerText = a.value;
    }
    // 利用者識別番号
    alert();
    document.getElementById("maskat_widget_dojo_84").value = myid1_save;
    document.getElementById("maskat_widget_dojo_85").value = myid2_save;
    document.getElementById("maskat_widget_dojo_86").value = myid3_save;
    document.getElementById("maskat_widget_dojo_87").value = myid4_save;
    // パスワード
    document.getElementById("maskat_widget_dojo_96").value = mypwd_save;
  });
});
